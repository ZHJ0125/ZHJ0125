## Hi, there! 👋

+ 🌱 这里是张厚今的GitHub
+ 📚 本人目前就读于山东科技大学，电子信息科学与技术专业
+ 🎯 擅长嵌入式 Linux 和 C 语言编程，熟悉 Python、Qt 和 Android 开发
+ 💬 欢迎访问我的 [Gitee](https://gitee.com/zhj0125/), [Blog](https://zhj.forever305.cn) 和 [CSDN](https://me.csdn.net/ZHJ123CSDN)
<!-- [![Anurag's github stats](https://github-readme-stats.vercel.app/api?username=ZHJ0125&show_icons=true&icon_color=1E90FF)](https://github.com/ZHJ0125) -->
